<?php
// +----------------------------------------------------------------------
// | 公用控制器
// +----------------------------------------------------------------------
// | 深圳市君鉴测试仪器租赁有限公司
// +----------------------------------------------------------------------
// | Author: 卓战友 125323228@qq.com
// +----------------------------------------------------------------------

namespace Chrent\Controller;
use Think\Controller;
class PublicController extends \Think\Controller {
    /**
     * 后台用户登录
     * @author 卓战友
     */
    public function login($username = null, $password = null, $verify = null){
        if(IS_POST){
			/* 登录用户 */
			$Users = D('Users');
			if($Users->login($username, $password)){ //登录用户
				//TODO:跳转到登录前页面
				$this->success('登录成功！', U('Index/index'));
			} else {
				$this->error($Users->getError());
			}
        } else {
            if(is_login()){
                $this->redirect('Index/index');
            }else{
                /* 读取数据库中的配置 */
                $config	=	S('DB_CONFIG_DATA');
                if(!$config){
                    $config	=	D('Config')->lists();
                    S('DB_CONFIG_DATA',$config);
                }
                C($config); //添加配置
                
                $this->display();
            }
        }
    }

    /* 退出登录 */
    public function logout(){
        if(is_login()){
            D('users')->logout();
            session('[destroy]');
            $this->success('退出成功！', U('login'));
        } else {
            $this->redirect('login');
        }
    }

    public function verify(){
        $verify = new \Think\Verify();
        $verify->entry(1);
    }
}