<?php
// +----------------------------------------------------------------------
// | 菜单模型
// +----------------------------------------------------------------------
// | 深圳市君鉴测试仪器租赁有限公司
// +----------------------------------------------------------------------
// | Author: 卓战友 125323228@qq.com
// +----------------------------------------------------------------------

namespace Chrent\Model;
use Think\Model;

class MenuModel extends Model {

    protected $_validate = array(
        array('menu_name', 'require', '菜单名称不能为空', self::EXISTS_VALIDATE, 'regex'),
        array('url', 'require', '功能节点不能为空', self::EXISTS_VALIDATE, 'regex')
    );
}